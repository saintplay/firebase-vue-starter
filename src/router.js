import Vue from 'vue'
import Router from 'vue-router'
import List from './views/List.vue'

import store from '@/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'list',
      component: List,
      meta: {
        authRequired: true,
      },
    },
    {
      path: '/add',
      name: 'add',
      component: () => import(/* webpackChunkName: "about" */ './views/Add.vue'),
      meta: {
        authRequired: true,
      },
    },
    {
      path: '/user',
      name: 'user',
      component: () => import(/* webpackChunkName: "about" */ './views/User.vue'),
    },
  ],
})

router.beforeEach((routeTo, routeFrom, next) => {
  const authRequired = routeTo.matched.some(route => route.meta.authRequired)
  if (!authRequired) return next()
  if (store.getters['loggedIn']) {
    return next()
  }
  return next({ name: 'user' })
})

export default router
