import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { db, auth } from '@/services/firebase'

const tasksRef = db.collection('tasks')
const usersRef = db.collection('users')

const staticState = {
  unsuscriber: null,
}

const unsubscribeToRealtimeUpdates = () => {
  staticState.unsuscriber && staticState.unsuscriber()
  staticState.unsuscriber = null
}

const subscribeToRealtimeUpdates = commit => {
  staticState.unsuscriber = tasksRef.onSnapshot(qs => {
    const tasks = qs.docs.map(d => ({ id: d.id, data: d.data() }))
    commit('setTasks', { tasks })
  })
}

export const getUserByUid = async uid => {
  const userSnapshot = await usersRef.doc(uid).get()
  if (userSnapshot.exists) return userSnapshot.data()
  return null
}

export default new Vuex.Store({
  state: {
    tasks: [],
    currentUser: null,
  },
  getters: {
    loggedIn: state => !!state.currentUser,
  },
  mutations: {
    setCurrentUser: (state, newUser) => {
      state.currentUser = newUser
    },
    setTasks: (state, { tasks }) => {
      state.tasks = tasks
    },
  },
  actions: {
    login: async ({ commit }, { email, password }) => {
      const credentiol = await auth.signInWithEmailAndPassword(email, password)
      const uid = credentiol.user.uid
      const userData = await getUserByUid(uid)
      const user = {
        uid,
        email,
        ...userData,
      }
      return commit('setCurrentUser', user)
    },
    logout: async ({ commit }) => {
      unsubscribeToRealtimeUpdates()
      await auth.signOut()
      commit('setCurrentUser', null)
    },
    fetchTasks: async ({ commit }) => {
      if (!staticState.unsuscriber) {
        subscribeToRealtimeUpdates(commit)
      }
      await tasksRef.get()
    },
    addNewTaskToDB: async (context, { taskData }) => {
      await tasksRef.add(taskData)
    },
  },
})
