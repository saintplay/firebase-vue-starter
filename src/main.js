import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

import App from './App.vue'
import router from './router'
import store, { getUserByUid } from './store'
import { auth } from '@/services/firebase'

Vue.config.productionTip = false

let app

auth.onAuthStateChanged(
  async user => {
    if (user) {
      const { uid, email } = user
      try {
        const [userData] = await Promise.all([getUserByUid(uid)])
        store.commit('setCurrentUser', { uid, email, ...userData })
      } catch (error) {
        store.commit('setCurrentUser', null)
      }
    }

    if (!app) {
      new Vue({
        router,
        store,
        render: h => h(App),
      }).$mount('#app')
    }
  },
  () => {
    store.commit('setCurrentUser', null)
  }
)
