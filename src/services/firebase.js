import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyBXLRY5-vCsMv0ww0hnxbgLejlfcasX7q0',
  authDomain: 'finanzas-app-upc.firebaseapp.com',
  databaseURL: 'https://finanzas-app-upc.firebaseio.com',
  projectId: 'finanzas-app-upc',
  storageBucket: 'finanzas-app-upc.appspot.com',
  messagingSenderId: '638304357850',
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export const auth = firebase.auth()
export const db = firebase.firestore()
db.settings({ timestampsInSnapshots: true })

export default firebase
